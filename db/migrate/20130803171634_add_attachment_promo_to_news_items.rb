class AddAttachmentPromoToNewsItems < ActiveRecord::Migration
  def self.up
    change_table :news_items do |t|
      t.attachment :promo
    end
  end

  def self.down
    drop_attached_file :news_items, :promo
  end
end
