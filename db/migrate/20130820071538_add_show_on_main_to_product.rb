class AddShowOnMainToProduct < ActiveRecord::Migration
  def change
    add_column :products, :show_on_main, :boolean
  end
end
