class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.string :title
      t.string :link
      t.boolean :show_on_main

      t.timestamps
    end
  end
end
