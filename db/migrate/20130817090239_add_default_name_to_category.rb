class AddDefaultNameToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :default_name, :string
  end
end
