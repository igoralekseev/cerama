class ChangeDataTypeForPriceOfProducts < ActiveRecord::Migration
  def change
    change_column :products, :price_when_ordering, :decimal, :precision => 8, :scale => 2
    change_column :products, :price_from_warehouse, :decimal, :precision => 8, :scale => 2
  end
end
