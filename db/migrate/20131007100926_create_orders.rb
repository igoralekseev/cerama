class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.references :products

      t.timestamps
    end
    add_index :orders, :products_id
  end
end
