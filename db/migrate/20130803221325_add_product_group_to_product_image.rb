class AddProductGroupToProductImage < ActiveRecord::Migration
  def change
    change_table :product_images do |t|
      t.references :product_group, index: true 
    end
  end
  def down
    change_table :product_images do |t|
      t.remove :product_group_id
    end
  end
end

