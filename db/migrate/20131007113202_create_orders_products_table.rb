class CreateOrdersProductsTable < ActiveRecord::Migration
  def up
    remove_index :orders, :products_id
    remove_column :orders, :products_id

    create_table :orders_products, :id => false do |t|
        t.references :order
        t.references :product
    end

    add_index :orders_products, [:order_id, :product_id]
    add_index :orders_products, :order_id


  end

  def down
    drop_table :orders_products
  end
end
