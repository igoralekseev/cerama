class AddProductToProductImage < ActiveRecord::Migration
  def change
    change_table :product_images do |t|
      t.references :product, index: true 
    end
  end
  def down
    change_table :product_images do |t|
      t.remove :product_id
    end
  end
end
