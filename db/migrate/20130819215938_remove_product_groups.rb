# require_relative '20130803214932_create_product_groups'
# require_relative '20130803221325_add_product_group_to_product_image'

class RemoveProductGroups < ActiveRecord::Migration
  def change
    drop_table :product_groups 
    
    change_table :product_images do |t|
      t.remove :product_group_id
    end
  end
end


