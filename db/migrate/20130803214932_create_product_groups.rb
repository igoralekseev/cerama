class CreateProductGroups < ActiveRecord::Migration
  def change
    create_table :product_groups do |t|
      t.string :name
      t.text :description
      t.integer :quantity_in_stock
      t.decimal :price_from_warehouse, precision: 5, scale: 2
      t.decimal :price_when_ordering, precision: 5, scale: 2
      t.boolean :show_on_main
      t.references :category, index: true
      
      t.timestamps
    end
    add_index :product_groups, :category_id
  end
end
