class ChangeDefaultsInProducts < ActiveRecord::Migration
  def change
    change_column_default :products, :quantity_in_stock, 0
    change_column_default :products, :price_when_ordering, 0.00 
    change_column_default :products, :price_from_warehouse, 0.00
  end
end
