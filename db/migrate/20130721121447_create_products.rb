class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :descripton
      t.integer :quantity_in_stock
      t.decimal :price_when_ordering, precision: 5, scale: 2 
      t.decimal :price_from_warehouse, precision: 5, scale: 2

      t.timestamps
    end
  end
end
