class Order < ActiveRecord::Base
  has_and_belongs_to_many :products
  attr_accessible :email, :name, :phone, :product_ids

  # accepts_nested_attributes_for :products, allow_destroy: false
end
