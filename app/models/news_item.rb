class NewsItem < ActiveRecord::Base
  attr_accessible :link, :show_on_main, :title, :text, :promo, :promo_url
  has_attached_file :promo, :styles => { :full_width => "980x300#", :large => "600x300>", :thumb => "50x50#" },
    :url => "/assets/:id/:style/:basename.:extension",
    :path => ":rails_root/public/assets/:id/:style/:basename.:extension"
end
