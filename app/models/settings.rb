class Settings < ActiveRecord::Base
  class << self
    attr_accessor :store_attrs, :fake
  end 

  @store_attrs = [ :name, :phone1, :contacts, :email ]
  attr_accessible :data, :name, :phone1, :contacts, :email


  store :data, accessors: @store_attrs, coder: JSON
end
