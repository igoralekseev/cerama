class ProductGroup < ActiveRecord::Base
  belongs_to :category
  has_many :product_images
  attr_accessible :description, :name, :price_from_warehouse, :price_when_ordering, :quantity_in_stock, :show_on_main, :category_id, :product_images_attributes
  accepts_nested_attributes_for :product_images, allow_destroy: true
end
