class Page < ActiveRecord::Base
  attr_accessible :slug, :text, :title
end
