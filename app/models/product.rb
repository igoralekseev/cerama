class Product < ActiveRecord::Base
  has_and_belongs_to_many :orders

  has_many :product_images
  belongs_to :category
  attr_accessible :description, :name, :show_on_main, :price_from_warehouse, :price_when_ordering, :quantity_in_stock, :sku, :product_images_attributes, :category_id
  accepts_nested_attributes_for :product_images, allow_destroy: true

  def name_with_default
    n = read_attribute(:name)
    n = self.category.andand.default_name if n.nil? or n.empty?
    n
  end
end
