class Category < ActiveRecord::Base
  has_many :products
  has_many :product_groups
  attr_accessible :name, :order, :slug, :default_name, :show_on_main
end
