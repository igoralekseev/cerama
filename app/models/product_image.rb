class ProductImage < ActiveRecord::Base
  belongs_to :product
  attr_accessible :order, :image, :product_id, :image_url
  has_attached_file :image, :styles => { :full_width => "940x900>", :large => "600x400>", :medium => "220x200#", :thumb => "50x50#" },
    :url => "/assets/:id/:style/:basename.:extension",
    :path => ":rails_root/public/assets/:id/:style/:basename.:extension"
end

