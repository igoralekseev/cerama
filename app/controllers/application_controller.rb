class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_layout_variables
  def set_layout_variables
    @catalogue_categories = Category.where(show_on_main: true)
    @settings = Settings.first
  end
end
