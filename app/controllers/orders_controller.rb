class OrdersController < ApplicationController
  def index

  end

  def create
    #create
    @order = Order.create(params[:order])
    
    respond_to do |format|
      if @order.save
        OrderMailer.order_notification(@order).deliver
        format.json { render :json => @order }
      else
        format.json { render :json => @order.errors }
      end
    end
  end
end
