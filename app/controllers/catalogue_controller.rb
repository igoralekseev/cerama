class CatalogueController < ApplicationController
  def category
    @category = Category.find_by_slug(params[:category_slug])
  end

  def product
    # @category = Category.find(params[:category_id])
    @product = Product.find(params[:product_id])
  end
end
