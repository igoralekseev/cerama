class PagesController < ApplicationController
  def show
    @page = Page.find_by_slug!(params[:page_slug])
  end

  def index
    @news = NewsItem.where(show_on_main: true)
    @products = Product.where(show_on_main: true)
  end
  
  def news
    @news = NewsItem.all() 
  end

  def news_item
    @news_item = NewsItem.find(params[:news_item_id])
  end

end
