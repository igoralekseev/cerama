class OrderMailer < ActionMailer::Base
  default from: "ceramahost@igoralekseev.info"

  def order_notification(order)
    @order = order
    mail(to: Settings.first.email, subject: "Запрос с сайта hostmebel")
  end
end
