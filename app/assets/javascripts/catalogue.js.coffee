$ ->
  $('.gallerie').each ->
    gallerie = $ this
    large = gallerie.find('.large a').fancybox()
    small = gallerie.find('.small div')
    small.click ->
      current = large.filter(".#{ $(this).data('image') }").show()
      large.not(current).hide()
      small.removeClass 'active'
      $(this).addClass 'active'

    small.first().click()

    
  $('a.order').each ->
    form = $ $(this).attr('href')
    $(this).fancybox
      content: form
      onStart: ->
        form.find(':input').attr('disabled', false)
        form.find('.errors').hide()


  $('body').on 'submit', 'form', (e) ->
    form = $ e.target
    return unless form.is '#order-create'
      
    e.preventDefault()
    errors = form.find('.errors').hide()

    phone = form.find('#order_phone').val()
    hasPhone = phone.replace(/\D/,'').length > 5
    
    email = form.find('#order_email').val()
    hasEmail = email.match(/^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/)
  
    unless hasEmail or hasPhone
      return errors.text('Заполните полe "Телефон" или "E-mail"').slideDown()
   
    
    data = form.serialize()
    form.find(':input').attr('disabled', true)
    $.fancybox.showActivity()
 

    error = ->
      form.find(':input').attr('disabled', false)
      $.fancybox.hideActivity()
      errors.text('Произошла ошибка. Попробуйте, пожалуйста, еще раз').slideDown()

    success = (data) ->
      if data.id isnt undefined
        $.fancybox "
            <div id='order-result'>
            <h3>Благодарим Вас!</h3> Номер Вашего обращения: <strong> #{ data.id}</strong>.<br>
            Наши менеджеры свяжутся с Вами в ближайшее время
            </div>
          ",
          onClose: -> 1
      else
        error()


    $.ajax
      url: form[0].action
      method: form[0].method || 'GET'
      data: data
      success: success
      error: error
                




    
    
