$ ->
  b = $('.slideshow')
  b.slidesjs
    width: b.width()
    height: 300
    navigation:
      active: false
    play:
      auto: true
      pauseOnHover: true
