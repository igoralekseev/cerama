ActiveAdmin.register Product do

  form html: { multipart: true }  do |f|
   f.inputs "Product information" do
     f.input :category
     f.input :name
     f.input :show_on_main
     f.input :sku
     f.input :quantity_in_stock
     f.input :price_when_ordering
     f.input :price_from_warehouse
     f.input :description, input_html: { id: "redactor_here", class: "redactor" }
   end

   f.inputs "Product images" do
     f.has_many :product_images do |pi|
       pi.input :order
       pi.input :image, :as => :file #, :label => :image, :hint => pi.template.image_tag(pi.object.image.url(:thumb))
       pi.input :_destroy, :as=>:boolean, :required => false, :label => 'Remove image'
     end 
   end
    f.buttons
  end  


  index do
    column :id, :sortable => :id do |p| 
      link_to(p.id, admin_product_path(p))
    end

    column :name, :sortable =>:name do |p| 
      link_to(p.name, admin_product_path(p))
    end

    column :price_when_ordering
    column :price_from_warehouse
    column :sku
    column do |p|
      raw p.product_images.map { |pi|
         image_tag(pi.image.url(:thumb))
      }.join(' ')

    end
  end

    show do |p|
      attributes_table do
        row :category
        row :id 
        row :name
        row :sku
        row :description
        row :quantity_in_stock
        row :price_when_ordering
        row :price_from_warehouse

        row :images do
          raw p.product_images.map { |pi|
             image_tag(pi.image.url(:thumb))
          }.join(' ')
        end
      end
      active_admin_comments
    end

end
