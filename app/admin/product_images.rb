ActiveAdmin.register ProductImage do
  # belongs_to :product

  form :html => { :enctype => "multipart/form-data" } do |f|
     f.inputs "Details" do
      f.input :product
      f.input :order
      f.input :image, :as => :file, :hint => f.template.image_tag(f.object.image.url(:medium))
    end
    f.buttons
   end

  index do
    column :image do |pi|
      link_to(image_tag(pi.image.url(:thumb)), admin_product_image_path(pi))
     end

    default_actions
  end

end
  
