ActiveAdmin.register Order do
  menu false

  form do |f|  
         f.inputs "Order Details" do  
              f.input :name  
              f.input :phone
              f.input :email
         end  
         f.inputs "Order Products" do  
              f.input :products, :as => :select, :input_html => { :multiple => true }  
         end  
         f.buttons
    end    

  index do

     selectable_column
     column :id, :sortable => :id do |p| 
          link_to(p.id, admin_order_path(p))
        end

     column :name
     column :phone 
     column :email
      
     

     column :products do |order|
        order.products.count.to_s + ': ' +  order.products.map { |p| p.name }.join(', ')
     end
    default_actions
  end


end
