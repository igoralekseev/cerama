ActiveAdmin.register NewsItem do

  index do
    column :id, :sortable => :id do |p| 
      link_to(p.id, admin_news_item_path(p))
    end

    column :title, :sortable =>:name do |p| 
      link_to(p.title, admin_news_item_path(p))
    end

    column :image do |p|
      raw image_tag(p.promo.url(:thumb))
    end

    column :text do |p|
      p.text.slice(0,100) + '...'
    end
    
    column :link
  end


  form html: { multipart: true }  do |f|
    f.inputs  do
      f.input :title
      f.input :promo, :as => :file, :label => :promo, :hint => f.template.image_tag(f.object.promo.url(:thumb))
      f.input :text, input_html: { id: "redactor_here", class: "redactor" }
      f.input :show_on_main
      f.input :link
    end

    f.buttons
  end  
end
