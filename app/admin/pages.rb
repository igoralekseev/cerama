ActiveAdmin.register Page do

  form html: { multipart: true }  do |f|
    f.inputs  do
      f.input :title
      f.input :slug
      f.input :text, input_html: { id: "redactor_here", class: "redactor" }

    end


    f.buttons
  end


  


end
