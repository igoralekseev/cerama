ActiveAdmin.register Settings do
  controller do
    skip_before_filter :set_layout_variables
  end


  attrs = Settings.store_attrs

  show do |s|

    attributes_table do
      attrs.each do |key|
        row key do
          raw s.data[key]
        end
      end
    end

  end


  form  do |f|
    f.inputs 'Settings' do
      attrs.each { |key|
        f.input key.to_sym , :as => :string
      }

      f.input :id, :as => :hidden
    end
    f.buttons
  end

end
