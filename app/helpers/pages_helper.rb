module PagesHelper
  def news_item_path(news_item)
    if !news_item.link.strip().empty?
      news_item.link
    elsif !news_item.text.empty?
      super news_item
    else 
      ''
    end
  end
end
