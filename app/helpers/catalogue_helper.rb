module CatalogueHelper
  def category_path(c)
    if c.respond_to? 'slug'
      super c.slug
    else
      super c
    end
  end
  
  def product_path(p)
    if p.category
      category_product_path(p.category.slug, p) 
    else
      super p
    end
  end
end
