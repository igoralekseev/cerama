{:ru => 
  { :i18n => 
    { :plural => 
      { :keys => [:one, :few, :other],
        :rule => lambda { |n| 
          if [1].include?(n % 10) && ![11].include?(n % 100)
            :one
          elsif [2, 3, 4].include?(n % 10) && ![12,13,14].include?(n % 100)
            :few 
          else
            :other 
          end
        } 
      } 
    } 
  } 
}
