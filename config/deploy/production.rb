
set :domain, "23.92.30.95" 

role :web, domain
role :app, domain
role :db,  domain, :primary => true

set :repository,      "git@bitbucket.org:igoralekseev/cerama.git"
