
set :domain, "176.58.98.136"

role :web, domain
role :app, domain
role :db,  domain, :primary => true

set :repository,      "git@bitbucket.org:igoralekseev/cerama.git"
