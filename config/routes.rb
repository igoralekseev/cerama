Cerama::Application.routes.draw do


  get "orders/index"
  post "orders/create"

  get 'catalogue/:category_slug', to: 'catalogue#category', as: 'category'
  get 'catalogue/:category_slug/:product_id', to: 'catalogue#product', as: 'category_product'

  get 'pages/:page_slug', to: 'pages#show', as: 'page'

  get 'news', to: 'pages#news', as: 'news'
  get 'news/:news_item_id', to: 'pages#news_item' , as: 'news_item'

  root :to => 'pages#index'


  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
end
