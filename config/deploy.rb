require "bundler/capistrano"
require 'capistrano-unicorn'


require 'capistrano/ext/multistage'
set :stages, %w(production staging)
set :default_stage, "staging"

# require "capistrano-rbenv"
# set :rbenv_ruby_version, "2.0.0-p247"


set :shared_children, shared_children + %w{public/uploads tmp/sockets tmp/pids }
ssh_options[:forward_agent] = true
default_run_options[:pty] = true


set :scm, :git
set :deploy_via, :remote_cache
set :deploy_to, '/srv/cerama'
set :bundle_flags, "--deployment --quiet --binstubs"


set :user,            "deployer"
set :group,           "webusers"
set :use_sudo,        false


ssh_options[:compression] = "none"

# set(:latest_release)  { fetch(:current_path) }
# set(:release_path)    { fetch(:current_path) }
# set(:current_release) { fetch(:current_path) }
# 
# set(:current_revision)  { capture("cd #{current_path}; git rev-parse --short HEAD").strip }
# set(:latest_revision)   { capture("cd #{current_path}; git rev-parse --short HEAD").strip }
# set(:previous_revision) { capture("cd #{current_path}; git rev-parse --short HEAD@{1}").strip }

# set :default_environment, {
#   'PATH' => "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
# }

default_environment["RAILS_ENV"] = 'production'
default_environment["PATH"]         = "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
# default_environment["GEM_HOME"]     = "--"
# default_environment["GEM_PATH"]     = "--"
# default_environment["RUBY_VERSION"] = "ruby-1.9.2-p290"



# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# default_run_options[:shell] = 'bash'



namespace :deploy do
  # desc "Start unicorn"
  # task :start do
  #   run "cd #{current_path} ; bundle exec unicorn_rails -c #{ current_path }/config/unicorn.rb -D"
  # end

  # # desc "Stop unicorn"
  # task :stop do
  #   run "kill -s QUIT `cat #{ current_path }/tmp/pids/unicorn.pid`"
  # end

  # task :restart do
  #   run "kill -s USR2 `cat #{ current_path }/tmp/pids/unicorn.pid`"
  # end

  # after 'deploy:restart', 'unicorn:reload' # app IS NOT preloaded
  after 'deploy:restart', 'unicorn:restart'  # app preloaded

  # before "deploy:assets:precompile" do
  #   # run "chown -R deployer:webusers #{ latest_release }/public/assets"
  #   run "ls -la #{ latest_release }/public/assets"

  # end
end

